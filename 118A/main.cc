#include <iostream>
#include <string>
#include <stdio.h>

inline int AsciiAlphaToLower(char c) {
    return c | 0x20;
}

int main(int argc, const char* argv[])
{
    std::string word;
    std::cin >> word;
    for (int i = 0; i < word.size(); i++)
    {
        char c = AsciiAlphaToLower(word[i]);
        if (c == 'a' || c == 'o' || c == 'y' || c == 'e' || c == 'u' || c == 'i')
        {
            continue;
        }
        putchar('.');
        putchar(c);
    }
    putchar('\n');
    return 0;
}
