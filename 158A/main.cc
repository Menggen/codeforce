#include <iostream>
#include <stdio.h>

int scores[52] = {};

int main(int argc, const char* argv[])
{
    int n, k;
    std::cin >> n >> k;
    for (int i = 0; i < n; i++)
    {
        std::cin >> scores[i];
    }

    int pos = 0;
    for (; scores[pos] > 0 && scores[pos] >= scores[k-1]; pos++);
    std::cout << pos;
    return 0;
}
