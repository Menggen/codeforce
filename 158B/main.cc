#include <iostream>
#include <stdio.h>

int counting[4] = {};

int main(int argc, const char* argv[])
{
    int group_size = 0;
    int n;
    std::cin >> n;
    for (int i = 0; i < n; i++)
    {
        std::cin >> group_size;
        counting[group_size-1]++;
    }
    
    int num_taxis = counting[3] + counting[1]/2 + counting[2];
    if (counting[0] >= counting[2])
    {
        int rest = counting[0] - counting[2] + (counting[1] % 2) * 2;
        num_taxis += rest / 4;
        if (rest % 4)
        {
            num_taxis++;
        }
    }
    else
    {
        num_taxis += (counting[1] % 2);
    }
    std::cout << num_taxis << std::endl;
    return 0;
}