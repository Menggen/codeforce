import sys

def get_piece(n, a):
	n_piece = int(n/a)
	if n % a != 0:
		n_piece += 1
	return n_piece
		
def main():
	n, m, a = map(int, sys.stdin.read().split())
	print(get_piece(n, a) * get_piece(m, a))
	
if __name__ == '__main__':
    sys.exit(main())
