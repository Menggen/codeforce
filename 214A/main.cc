#include <iostream>
#include <stdio.h>

int main(int argc, const char* argv[])
{
    int n, m;
    std::cin >> n >> m;

    int count = 0;
    int a, b;
	// Loop on square term to speed up
    for (a = 0; a*a <= n; a++)
    {
        b = n - a*a;
        if (a + b*b == m)
        {
            count++;
        }
    }
    std::cout << count << std::endl;
    return 0;
}