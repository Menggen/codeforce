#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

struct Point
{
    long long x_;
    long long y_;
    Point() : x_(0), y_(0) { }
    Point(long long x, long long y) : x_(x), y_(y) { }
};

int main(int argc, const char* argv[])
{
    long long a, b;
    std::cin >> a >> b;
    
    std::string cmd;
    std::cin >> cmd;
    std::vector<Point> pos_list;
    pos_list.push_back(Point(a, b));

    Point pos(0, 0);
    for (std::string::const_iterator it = cmd.begin(); it != cmd.end(); it++)
    {
        char c = *it;
        if (c == 'U')
        {
            pos.y_++;
        }
        else if (c == 'D')
        {
            pos.y_--;
        }
        else if (c == 'L')
        {
            pos.x_--;
        }
        else if (c == 'R')
        {
            pos.x_++;
        }
        pos_list.push_back(Point(a - pos.x_, b - pos.y_));
    }

    bool found = false;
    long long t;
    for (std::vector<Point>::const_iterator it = pos_list.begin(); 
        it != pos_list.end(); it++)
    {
        if (pos.x_ && (it->x_ % pos.x_ == 0))
        {
            t = it->x_ / pos.x_;
            if (t >= 0 && t * pos.y_ == it->y_)
            {
                found = true;
                break;
            }
        }
        else if ((it->x_ == 0) && pos.y_ && (it->y_ % pos.y_ == 0))
        {
            t = it->y_ / pos.y_;
            if (t >= 0)
            {
                found = true;
                break;
            }
        }
        else if ((it->x_ == 0) && (it->y_ == 0))
        {
            found = true;
            break;
        }
    }
    if (found)
    {
        puts("Yes");
    }
    else
    {
        puts("No");
    }

    return 0;
}