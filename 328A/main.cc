#include <iostream>
#include <stdio.h>

long long s[4] = {};
long long d;
long long q_n, q_d;

bool IsArithmetic()
{
    d = s[1] - s[0];
    for (int i = 2; i < 4; i++)
    {
        if (s[i]-s[i-1] != d)
        {
            return false;
        }
    }
    return true;
}

bool IsGeometric()
{
    if (s[0] == 0 || s[1] == 0 || s[0] == s[1])
    {
        return false;
    }
    q_n = s[1];
    q_d = s[0];
    for (int i = 2; i < 4; i++)
    {
        if (s[i-1]*q_n != s[i]*q_d)
        {
            return false;
        }
    }
    return true;
}

int main(int argc, const char* argv[])
{
    for (int i = 0; i < 4; i++)
    {
        std::cin >> s[i];
    }
    if (IsArithmetic())
    {
        std::cout << s[3] + d << std::endl;
    }
    else if (IsGeometric() && (s[3]*q_n % q_d == 0))
    {
        std::cout << s[3]*q_n/q_d << std::endl;
    }
    else
    {
        puts("42");
    }
    return 0;
}