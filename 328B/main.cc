#include <iostream>
#include <string>
#include <stdio.h>

int main(int argc, const char* argv[])
{
    std::string dest, src;
    std::cin >> dest >> src;

    int dest_digits[10] = {};
    for (std::string::iterator it = dest.begin(); it != dest.end(); it++)
    {
        if (*it == '2' || *it == '5')
        {
            dest_digits[2]++;
            dest_digits[5]++;
        }
        else if (*it == '6' || *it == '9')
        {
            dest_digits[6]++;
            dest_digits[9]++;
        }
        else
        {
            dest_digits[*it - '0']++;
        }
    }
    int src_digits[10] = {};
    for (std::string::iterator it = src.begin(); it != src.end(); it++)
    {
        if (*it == '2' || *it == '5')
        {
            src_digits[2]++;
            src_digits[5]++;
        }
        else if (*it == '6' || *it == '9')
        {
            src_digits[6]++;
            src_digits[9]++;
        }
        else
        {
            src_digits[*it - '0']++;
        }
    }

    int min_count = 200;
    int count;
    for (int i = 0; i < 10; i++)
    {
        if (dest_digits[i])
        {
            count = src_digits[i]/dest_digits[i];
            if (count < min_count)
            {
                min_count = count;
            }
        }
    }
    std::cout << min_count << std::endl;

    return 0;
}