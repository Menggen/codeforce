import sys
	
def main():
	n = int(sys.stdin.read().strip())
	if n > 2 and n % 2 == 0:
		print('YES')
	else:
		print('NO')
	
if __name__ == '__main__':
    sys.exit(main())
