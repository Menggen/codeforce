#include <iostream>
#include <string>
#include <stdio.h>

int main(int argc, const char* argv[])
{
    std::string word;
    int word_len;
    int num_testcases;
    std::cin >> num_testcases;
    while (num_testcases--)
    {
        std::cin >> word;
        word_len = word.size();
        if (word_len <= 10)
        {
            std::cout << word << std::endl;
        }
        else
        {
            std::cout << word[0] << (word_len - 2) << word[word_len-1] << 
                std::endl;
        }
    }
    return 0;
}
